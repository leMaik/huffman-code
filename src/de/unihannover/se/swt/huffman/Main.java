package de.unihannover.se.swt.huffman;

import de.unihannover.se.swt.huffman.analyzers.AlphabetAnalyzer;
import de.unihannover.se.swt.huffman.analyzers.CharacterFrequency;
import de.unihannover.se.swt.huffman.code.Code;
import de.unihannover.se.swt.huffman.code.UnknownCharacterException;
import de.unihannover.se.swt.huffman.huffmangenerator.HuffmanCodeGenerator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * An example application of {@link de.unihannover.se.swt.huffman.huffmangenerator.HuffmanCodeGenerator}.
 */
public class Main {
    /**
     * The entry point of this application.
     *
     * @param args Command line arguments
     */
    public static void main(String[] args) {
        HuffmanCodeGenerator generator = new HuffmanCodeGenerator();

        //Example text, taken from "The Metamorphosis" by Franz Kafka
        String input = "One morning, as Gregor Samsa was waking up from anxious dreams, he discovered that in his " +
                "bed he had been changed into a monstrous verminous bug. He lay on his armour-hard back and saw, as " +
                "he lifted his head up a little, his brown, arched abdomen divided up into rigid bow-like sections. " +
                "From this height the blanket, just about ready to slide off completely, could hardly stay in place. " +
                "His numerous legs, pitifully thin in comparison to the rest of his circumference, flickered " +
                "helplessly before his eyes." +
                "\"What’s happened to me?\" he thought. It was no dream. His room, a proper room for a human being, " +
                "only somewhat too small, lay quietly between the four well-known walls. Above the table, on which " +
                "an unpacked collection of sample cloth goods was spread out—Samsa was a travelling salesman—hung " +
                "the picture which he had cut out of an illustrated magazine a little while ago and set in a pretty " +
                "gilt frame. It was a picture of a woman with a fur hat and a fur boa. She sat erect there, lifting " +
                "up in the direction of the viewer a solid fur muff into which her entire forearm had disappeared.";

        //Add all alphabetic characters of that text, case insensitive
        for (CharacterFrequency character : AlphabetAnalyzer.analyze(input, false).getCharacters()) {
            generator.addCharacter(character);
        }

        Code code = generator.generateCode();

        //code.getCharacters() would be enough, but we want to have it sorted alphabetically
        List<String> sortedCharacters = new ArrayList<>(code.getCharacters());
        Collections.sort(sortedCharacters);

        //Print characters and codes
        for (String character : sortedCharacters) {
            try {
                System.out.println(String.format("'%s (%d occurrences)\t= %s",
                        character, generator.getFrequency(character), code.getCodeFor(character)));
            } catch (UnknownCharacterException e) {
                //should never happen here, as we only request codes and frequencies for characters that exist
                e.printStackTrace();
            }
        }
    }
}
