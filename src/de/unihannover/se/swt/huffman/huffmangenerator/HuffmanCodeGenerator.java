package de.unihannover.se.swt.huffman.huffmangenerator;

import de.unihannover.se.swt.huffman.analyzers.CharacterFrequency;
import de.unihannover.se.swt.huffman.code.UnknownCharacterException;

import java.util.*;

/**
 * A generator for optimal huffman codes.
 */
public class HuffmanCodeGenerator {
    private Map<String, Integer> characterFrequencies;

    /**
     * Creates a new huffman code generator.
     */
    public HuffmanCodeGenerator() {
        characterFrequencies = new HashMap<>();
    }

    /**
     * Adds the given character with the given frequency. If the character is already added, it is replaced.
     *
     * @param character The character to add
     * @param frequency The frequency of this character
     */
    public void addCharacter(String character, int frequency) {
        characterFrequencies.put(character, frequency);
    }

    /**
     * Adds the given character-frequency tuple. If the character is already added, it is replaced.
     *
     * @param character Character-frequency tuple to add
     */
    public void addCharacter(CharacterFrequency character) {
        addCharacter(character.getCharacter(), character.getFrequency());
    }

    /**
     * Generates the optimal huffman code using the list of characters and frequencies that were previously added.
     * <p/>
     * Note that this method doesn't invalidate this instance, so you may add more characters and then generate a new code using this object.
     *
     * @return An optimal huffman code for the given characters and frequencies
     */
    public HuffmanCode generateCode() {
        List<HuffmanCodeTreeNode> nodes = new ArrayList<>();
        for (Map.Entry<String, Integer> c : characterFrequencies.entrySet())
            nodes.add(new HuffmanCodeTreeNode(c.getKey(), c.getValue()));

        while (nodes.size() > 1) { //while we have more than one root node...
            //...sort the nodes by frequency, in ascending order...
            Collections.sort(nodes, new NodeFrequencyComparator());

            //...and replace the two first nodes by the merged node of that two nodes (see mergeNodes for more information on that)
            nodes.set(0, mergeNodes(nodes.get(0), nodes.get(1)));
            nodes.remove(1); //"replace first two nodes by one node" is the same as "replace first node and remove second node"
        }

        return new HuffmanCode(nodes.get(0));
    }

    /**
     * Merges the two nodes to a new node that has the sum of the frequencies as frequency and a combination of the
     * characters of both nodes as character. The children of the returned node are the two given nodes.
     * <p/>
     * This is one step of the used algorithm to create a huffman code.
     *
     * @param n1 First node to merge
     * @param n2 Second node to merge
     * @return Merged node, frequency is sum of both frequencies, children are the two given nodes
     */
    private static HuffmanCodeTreeNode mergeNodes(HuffmanCodeTreeNode n1, HuffmanCodeTreeNode n2) {
        HuffmanCodeTreeNode merged = new HuffmanCodeTreeNode(
                n1.getCharacter() + "/" + n2.getCharacter(),
                n1.getFrequency() + n2.getFrequency());
        merged.setFirstChild(n1);
        merged.setSecondChild(n2);
        return merged;
    }

    /**
     * Gets the frequency of the given character. If the character is unknown, zero is returned.
     *
     * @param character The character to get the frequency of
     * @return The frequency of the given character
     * @throws de.unihannover.se.swt.huffman.code.UnknownCharacterException If the character is unknown
     */
    public int getFrequency(String character) throws UnknownCharacterException {
        if (characterFrequencies.containsKey(character))
            return characterFrequencies.get(character);
        throw new UnknownCharacterException("'" + character + "' has not been added to the generator.");
    }

    /**
     * A comparator that compares huffman tree nodes by frequency.
     *
     * @see de.unihannover.se.swt.huffman.huffmangenerator.HuffmanCodeTreeNode
     */
    private static class NodeFrequencyComparator implements java.util.Comparator<HuffmanCodeTreeNode> {
        @Override
        public int compare(HuffmanCodeTreeNode a, HuffmanCodeTreeNode b) {
            return a.getFrequency() - b.getFrequency();
        }
    }
}
