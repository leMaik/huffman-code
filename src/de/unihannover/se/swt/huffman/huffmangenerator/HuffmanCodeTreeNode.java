package de.unihannover.se.swt.huffman.huffmangenerator;

/**
 * A node in the code tree.
 */
public class HuffmanCodeTreeNode {
    private String character;
    private int frequency;

    private HuffmanCodeTreeNode parent;
    private HuffmanCodeTreeNode firstChild;
    private HuffmanCodeTreeNode secondChild;

    /**
     * Creates a node with a character and a frequency.
     *
     * @param parent    Parent node
     * @param character Character of this node
     * @param frequency Frequency of this node
     */
    public HuffmanCodeTreeNode(HuffmanCodeTreeNode parent, String character, int frequency) {
        this.parent = parent;
        this.character = character;
        this.frequency = frequency;
    }

    /**
     * Creates a root node.
     *
     * @param character Character of this node
     * @param frequency Frequency of this node
     */
    public HuffmanCodeTreeNode(String character, int frequency) {
        this(null, character, frequency);
    }

    /**
     * Sets the first child node of this node.
     *
     * @param firstChild Node to set as first child of this node
     */
    public void setFirstChild(HuffmanCodeTreeNode firstChild) {
        this.firstChild = firstChild;
        firstChild.parent = this;
    }

    /**
     * Sets the second child node of this node.
     *
     * @param secondChild Node to set as second child of this node
     */
    public void setSecondChild(HuffmanCodeTreeNode secondChild) {
        this.secondChild = secondChild;
        secondChild.parent = this;
    }

    /**
     * Gets the path of this node. That is the edge labels of all edges from this node to the root node, concatenated.
     *
     * @return The path of this node
     */
    public String getPath() {
        if (parent == null)
            return "";
        if (parent.firstChild == this)
            return parent.getPath() + "1";
        else if (parent.secondChild == this)
            return parent.getPath() + "0";
        else
            throw new IllegalStateException("Node is not child of its parent.");
    }

    /**
     * Gets the character of this node.
     *
     * @return The character of this node
     */
    public String getCharacter() {
        return character;
    }

    /**
     * Gets the frequency of this node.
     *
     * @return The frequency of this node
     */
    public int getFrequency() {
        return frequency;
    }

    /**
     * Gets the first child node of this node.
     *
     * @return The first child node of this node
     */
    public HuffmanCodeTreeNode getFirstChild() {
        return firstChild;
    }

    /**
     * Gets the second child node of this node.
     *
     * @return The second child node of this node
     */
    public HuffmanCodeTreeNode getSecondChild() {
        return secondChild;
    }
}
