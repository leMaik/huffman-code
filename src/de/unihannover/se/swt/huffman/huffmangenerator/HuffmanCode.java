package de.unihannover.se.swt.huffman.huffmangenerator;

import de.unihannover.se.swt.huffman.code.Code;
import de.unihannover.se.swt.huffman.code.UnknownCharacterException;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * A huffman code for a set of characters with specific frequencies.
 */
public class HuffmanCode implements Code {
    private Map<String, String> codes; //binary codes are saved as strings of zeros and ones

    /**
     * Creates a new huffman code from the given tree.
     *
     * @param huffmanCodeTreeNode The root node of the tree to generate the code from
     */
    HuffmanCode(HuffmanCodeTreeNode huffmanCodeTreeNode) {
        codes = new HashMap<>();
        extractCodes(huffmanCodeTreeNode);
    }

    /**
     * Extracts the codes from the given node and travels to child nodes to extract codes from there.
     *
     * @param node Node to extract the codes from
     */
    private void extractCodes(HuffmanCodeTreeNode node) {
        boolean hasChilds = false;
        if (node.getFirstChild() != null) {
            extractCodes(node.getFirstChild());
            hasChilds = true;
        }
        if (node.getSecondChild() != null) {
            extractCodes(node.getSecondChild());
            hasChilds = true;
        }
        if (!hasChilds) { //if the node is a leaf, it contains a character. So add that character and its code!
            codes.put(node.getCharacter(), node.getPath());
        }
    }

    @Override
    public Set<String> getCharacters() {
        return codes.keySet();
    }

    @Override
    public String getCodeFor(String character) throws UnknownCharacterException {
        if (!codes.containsKey(character))
            throw new UnknownCharacterException("'" + character + "' can't be encoded using this code.");
        return codes.get(character);
    }
}
