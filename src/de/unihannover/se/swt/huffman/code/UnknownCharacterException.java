package de.unihannover.se.swt.huffman.code;

/**
 * Exception that is thrown when a given character can't be used or encoded because it's unknown.
 */
public class UnknownCharacterException extends Exception {
    /**
     * Creates a new exception with the given message and inner exception.
     *
     * @param msg Message to display in the stack trace
     */
    public UnknownCharacterException(String msg) {
        super(msg);
    }
}
