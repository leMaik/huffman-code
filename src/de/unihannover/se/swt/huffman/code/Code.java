package de.unihannover.se.swt.huffman.code;

import java.util.Set;

/**
 * A code that can encode a set of characters using code words.
 */
public interface Code {

    /**
     * Gets the set of characters that can be encoded using this code.
     *
     * @return The set of characters that can be encoded using this code
     */
    public Set<String> getCharacters();

    /**
     * Gets the code for the given character.
     *
     * @param character The character to get the code word for
     * @return The code word for the given character
     * @throws de.unihannover.se.swt.huffman.code.UnknownCharacterException If the character is unknown to this code
     */
    public String getCodeFor(String character) throws UnknownCharacterException;
}
