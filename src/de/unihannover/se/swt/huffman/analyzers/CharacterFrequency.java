package de.unihannover.se.swt.huffman.analyzers;

/**
 * A character with an absolute frequency.
 */
public class CharacterFrequency {
    private String character;
    private int frequency;

    /**
     * Creates a new character-frequency tuple.
     *
     * @param character Character
     * @param frequency Absolute frequency of that character
     */
    public CharacterFrequency(String character, int frequency) {
        this.character = character;
        this.frequency = frequency;
    }

    /**
     * Gets the character this tuple contains.
     *
     * @return The character
     */
    public String getCharacter() {
        return character;
    }

    /**
     * Gets the absolute frequency of the character.
     *
     * @return The absolute frequency of the character
     */
    public int getFrequency() {
        return frequency;
    }
}
