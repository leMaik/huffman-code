package de.unihannover.se.swt.huffman.analyzers;

/**
 * Analyzer that analyzes the frequency of alphabetical characters in a given {@link java.lang.String}.
 */
public class AlphabetAnalyzer {
    /**
     * Analyzes the frequency of alphabetical (A-Z, a-z) characters in the given string.
     *
     * @param input         Input string to count the characters of
     * @param caseSensitive True to distinguish upper case and lower case characters, false to count them as the
     *                      same character
     * @return Map of characters and frequencies, based on the input string
     */
    public static CharacterFrequencyCounter analyze(String input, boolean caseSensitive) {
        CharacterFrequencyCounter map = new CharacterFrequencyCounter();

        for (char c : input.toCharArray()) {
            if (Character.isAlphabetic(c)) { //only count character if it's alphabetic
                if (caseSensitive) {
                    map.countCharacter(String.valueOf(c));
                } else {
                    //if case doesn't matter, we count every letter as uppercase letter
                    map.countCharacter(String.valueOf(c).toUpperCase());
                }
            }
        }

        return map;
    }
}
