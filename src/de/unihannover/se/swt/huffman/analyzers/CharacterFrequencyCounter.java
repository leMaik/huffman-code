package de.unihannover.se.swt.huffman.analyzers;

import de.unihannover.se.swt.huffman.code.UnknownCharacterException;

import java.util.*;

/**
 * A counter that is used for counting characters.
 */
public class CharacterFrequencyCounter {
    private Map<String, Integer> characters;

    /**
     * Creates a nwe character frequency map.
     */
    public CharacterFrequencyCounter() {
        characters = new HashMap<>();
    }

    /**
     * Gets the absolute frequency of the given character.
     *
     * @param character Character to get the frequency for
     * @return The absolute frequency of the given character
     */
    public int getFrequency(String character) {
        if (character.contains(character))
            return characters.get(character);
        else
            return 0; //character not seen yet
    }

    /**
     * Gets a list of all characters with their frequencies that were counted using this counter.
     *
     * @return A list of all characters and their frequencies
     * @see de.unihannover.se.swt.huffman.analyzers.CharacterFrequency
     */
    public Collection<CharacterFrequency> getCharacters() {
        List<CharacterFrequency> characterFrequencies = new ArrayList<>();

        for (Map.Entry<String, Integer> character : characters.entrySet())
            characterFrequencies.add(new CharacterFrequency(character.getKey(), character.getValue()));

        return characterFrequencies;
    }

    /**
     * Counts the given character.
     *
     * @param character The character to count
     */
    public void countCharacter(String character) {
        //If it the character was counted before, its frequency is increased. If not, its frequency is set to 1.
        if (characters.containsKey(character))
            characters.put(character, getFrequency(character) + 1);
        else
            characters.put(character, 1);
    }
}
